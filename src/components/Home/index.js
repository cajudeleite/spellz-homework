import './styles.scss';
import { Link } from 'react-router-dom';
import Logo from '../Logo';
import Arrow from './arrow.svg';

const Home = () => (
  <div className="home">
    <Logo />
    <h1 className="home__text slide-in-bck-bottom">Yo 👋<br /><br /><br />What's your <br />first name ?</h1>
    <input type="text" className="home__input slide-in-bck-bottom" placeholder="Ex : Shazam" />
    <Link to="/dashboard">
      <button type="button" className="home__skip slide-in-bck-left">Skip</button>
    </Link>
    <Link to="/dashboard">
      <button type="button" className="home__start slide-in-bck-right">Let's start<img className="home__start__arrow" src={Arrow} alt="" /></button>
    </Link>
  </div>
);

export default Home;
