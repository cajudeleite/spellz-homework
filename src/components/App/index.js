import './styles.scss';
import { Routes, Route } from 'react-router-dom';
import Home from '../Home';
import Dashboard from '../Dashboard';

const App = () => (
  <div className="app">
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/dashboard" element={<Dashboard />} />
    </Routes>
  </div>
);

export default App;
