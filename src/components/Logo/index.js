import './styles.scss';
import logo from './logo.svg';

const Logo = () => (
  <div className="logo slide-top">
    <img className="scale-down-center" src={logo} alt="Spellz' logo" />
  </div>
);

export default Logo;
