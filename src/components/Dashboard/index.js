import './styles.scss';
import { useState } from 'react';
import Lock from './lock.svg';
import Arrow from './arrow.svg';

const Dashboard = () => {
  const [items, setItems] = useState([]);
  const [cardsDisplay, setCardsDisplay] = useState({
    bigalert: 'flex',
    smallalert: 'none',
    filter: 'flex',
    itemCards: '2',
    addCard: '1',
    cosmeticButton: true,
  });
  return (
    <div className="dashboard">
      <div style={{ display: cardsDisplay.filter }} className="dashboard__filter" />
      <h1 className="dashboard__title slide-in-blurred-top">Dinner Recipe</h1>
      <button
        type="button"
        className="dashboard__question slide-in-blurred-top"
        onClick={() => {
          setCardsDisplay({
            bigalert: 'flex',
            smallalert: 'none',
            filter: 'flex',
            itemCards: '2',
            addCard: '1',
            cosmeticButton: true,
          });
        }}
      >?
      </button>
      <p className="dashboard__undertext slide-in-blurred-top">Here is a great recipe for your dinner </p>
      <div style={{ zIndex: cardsDisplay.itemCards }} className="dashboard__cards slide-in-elliptic-top-fwd">
        <div className="dashboard__cards__card">
          <img className="dashboard__cards__card__lock" src={Lock} alt="Lock" />
          <h6 className="dashboard__cards__card__text">Ingredients</h6>
        </div>
        <div className="dashboard__cards__card">
          <img className="dashboard__cards__card__lock" src={Lock} alt="Lock" />
          <h6 className="dashboard__cards__card__text">Preparation time</h6>
        </div>
        {items.map((item) => (
          <div className="dashboard__cards__card">
            <h6 className="dashboard__cards__card__text">{item}</h6>
          </div>
        ))}
        {!cardsDisplay.cosmeticButton && <button type="button" className="dashboard__cards__add">+ Add item</button>}
      </div>
      {cardsDisplay.cosmeticButton && <button style={{ zIndex: cardsDisplay.addCard }} type="button" className="dashboard__cards__add__cosmetic slide-in-elliptic-top-fwd">+ Add item</button>}
      <div style={{ display: cardsDisplay.bigalert }} className="dashboard__bigalert roll-in-blurred-left">
        <p className="dashboard__bigalert__text__bold">
          Any good recipe starts with good ingredients. Preparation time is of the utmost importance
        </p>
        <p className="dashboard__bigalert__text__emoji">🤓</p>
        <p className="dashboard__bigalert__text__simple">
          You can add any item you deem important to share your recipe with your community
        </p>
        <button
          type="button"
          className="dashboard__bigalert__button__done"
          onClick={() => {
            setCardsDisplay({
              ...cardsDisplay, bigalert: 'none', itemCards: '1', cosmeticButton: false, filter: 'none',
            });
          }}
        >Done!
        </button>
        <button
          type="button"
          className="dashboard__bigalert__button__add"
          onClick={() => {
            setCardsDisplay({
              ...cardsDisplay, bigalert: 'none', smallalert: 'block', itemCards: '1', addCard: '2',
            });
          }}
        >Add an item<img className="dashboard__bigalert__button__add__arrow" src={Arrow} alt="" />
        </button>
      </div>
      <div style={{ display: cardsDisplay.smallalert }} className="dashboard__smallalert roll-in-blurred-left">
        <p className="dashboard__smallalert__text__emoji">🤓</p>
        <p className="dashboard__smallalert__text__bold">
          Can't thing of anything? How about “cooking time”?
        </p>
        <button
          type="button"
          className="dashboard__smallalert__button"
          onClick={() => {
            setCardsDisplay({
              ...cardsDisplay, smallalert: 'none', addCard: '1', cosmeticButton: false, filter: 'none',
            });
          }}
        >Try it now !
        </button>
      </div>
    </div>
  );
};

export default Dashboard;
